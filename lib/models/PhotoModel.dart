class PhotoModel {
  String _status;
  int _kode;
  List<Data> _data;
  String _top;
  String _toplink;
  String _bottom;
  String _bottomlink;

  PhotoModel(
      {String status,
      int kode,
      List<Data> data,
      String top,
      String toplink,
      String bottom,
      String bottomlink}) {
    this._status = status;
    this._kode = kode;
    this._data = data;
    this._top = top;
    this._toplink = toplink;
    this._bottom = bottom;
    this._bottomlink = bottomlink;
  }

  String get status => _status;
  set status(String status) => _status = status;
  int get kode => _kode;
  set kode(int kode) => _kode = kode;
  List<Data> get data => _data;
  set data(List<Data> data) => _data = data;
  String get top => _top;
  set top(String top) => _top = top;
  String get toplink => _toplink;
  set toplink(String toplink) => _toplink = toplink;
  String get bottom => _bottom;
  set bottom(String bottom) => _bottom = bottom;
  String get bottomlink => _bottomlink;
  set bottomlink(String bottomlink) => _bottomlink = bottomlink;

  PhotoModel.fromJson(Map<String, dynamic> json) {
    _status = json['status'];
    _kode = json['kode'];
    if (json['data'] != null) {
      _data = new List<Data>();
      json['data'].forEach((v) {
        _data.add(new Data.fromJson(v));
      });
    }
    _top = json['top'];
    _toplink = json['toplink'];
    _bottom = json['bottom'];
    _bottomlink = json['bottomlink'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this._status;
    data['kode'] = this._kode;
    if (this._data != null) {
      data['data'] = this._data.map((v) => v.toJson()).toList();
    }
    data['top'] = this._top;
    data['toplink'] = this._toplink;
    data['bottom'] = this._bottom;
    data['bottomlink'] = this._bottomlink;
    return data;
  }
}

class Data {
  String _postId;
  String _postDate;
  String _postDateCreated;
  String _postTitle;
  String _postContent;
  String _thumb;
  List<PostImageContent> _postImageContent;
  String _postImageCaption;
  String _slug;
  String _editor;
  String _postSource;

  Data(
      {String postId,
      String postDate,
      String postDateCreated,
      String postTitle,
      String postContent,
      String thumb,
      List<PostImageContent> postImageContent,
      String postImageCaption,
      String slug,
      String editor,
      String postSource}) {
    this._postId = postId;
    this._postDate = postDate;
    this._postDateCreated = postDateCreated;
    this._postTitle = postTitle;
    this._postContent = postContent;
    this._thumb = thumb;
    this._postImageContent = postImageContent;
    this._postImageCaption = postImageCaption;
    this._slug = slug;
    this._editor = editor;
    this._postSource = postSource;
  }

  String get postId => _postId;
  set postId(String postId) => _postId = postId;
  String get postDate => _postDate;
  set postDate(String postDate) => _postDate = postDate;
  String get postDateCreated => _postDateCreated;
  set postDateCreated(String postDateCreated) =>
      _postDateCreated = postDateCreated;
  String get postTitle => _postTitle;
  set postTitle(String postTitle) => _postTitle = postTitle;
  String get postContent => _postContent;
  set postContent(String postContent) => _postContent = postContent;
  String get thumb => _thumb;
  set thumb(String thumb) => _thumb = thumb;
  List<PostImageContent> get postImageContent => _postImageContent;
  set postImageContent(List<PostImageContent> postImageContent) =>
      _postImageContent = postImageContent;
  String get postImageCaption => _postImageCaption;
  set postImageCaption(String postImageCaption) =>
      _postImageCaption = postImageCaption;
  String get slug => _slug;
  set slug(String slug) => _slug = slug;
  String get editor => _editor;
  set editor(String editor) => _editor = editor;
  String get postSource => _postSource;
  set postSource(String postSource) => _postSource = postSource;

  Data.fromJson(Map<String, dynamic> json) {
    _postId = json['post_id'];
    _postDate = json['post_date'];
    _postDateCreated = json['post_date_created'];
    _postTitle = json['post_title'];
    _postContent = json['post_content'];
    _thumb = json['thumb'];
    if (json['post_image_content'] != null) {
      _postImageContent = new List<PostImageContent>();
      json['post_image_content'].forEach((v) {
        _postImageContent.add(new PostImageContent.fromJson(v));
      });
    }
    _postImageCaption = json['post_image_caption'];
    _slug = json['slug'];
    _editor = json['editor'];
    _postSource = json['post_source'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['post_id'] = this._postId;
    data['post_date'] = this._postDate;
    data['post_date_created'] = this._postDateCreated;
    data['post_title'] = this._postTitle;
    data['post_content'] = this._postContent;
    data['thumb'] = this._thumb;
    if (this._postImageContent != null) {
      data['post_image_content'] =
          this._postImageContent.map((v) => v.toJson()).toList();
    }
    data['post_image_caption'] = this._postImageCaption;
    data['slug'] = this._slug;
    data['editor'] = this._editor;
    data['post_source'] = this._postSource;
    return data;
  }
}

class PostImageContent {
  String _photoId;
  String _photoContent;

  PostImageContent({String photoId, String photoContent}) {
    this._photoId = photoId;
    this._photoContent = photoContent;
  }

  String get photoId => _photoId;
  set photoId(String photoId) => _photoId = photoId;
  String get photoContent => _photoContent;
  set photoContent(String photoContent) => _photoContent = photoContent;

  PostImageContent.fromJson(Map<String, dynamic> json) {
    _photoId = json['photo_id'];
    _photoContent = json['photo_content'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['photo_id'] = this._photoId;
    data['photo_content'] = this._photoContent;
    return data;
  }
}
