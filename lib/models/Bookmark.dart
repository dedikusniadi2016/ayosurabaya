class Bookmark {
  int _id;
  String _postId;
  String _postTitle;
  String _postImage;

  Bookmark({int id, String postId, String postTitle, String postImage}) {
    this._id = id;
    this._postId = postId;
    this._postTitle = postTitle;
    this._postImage = postImage;
  }

  int get id => _id;
  set id(int id) => _id = id;
  String get postId => _postId;
  set postId(String postId) => _postId = postId;
  String get postTitle => _postTitle;
  set postTitle(String postTitle) => _postTitle = postTitle;
  String get postImage => _postImage;
  set postImage(String postImage) => _postImage = postImage;

  Bookmark.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _postId = json['post_id'];
    _postTitle = json['post_title'];
    _postImage = json['post_image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['post_id'] = this._postId;
    data['post_title'] = this._postTitle;
    data['post_image'] = this._postImage;
    return data;
  }

  Map<String, dynamic> toMap() => {
    "id": this._id,
    "post_id": this._postId,
    "post_title": this._postTitle,
    "post_image": this._postImage,
  };
}