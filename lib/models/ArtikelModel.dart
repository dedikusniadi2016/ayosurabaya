import 'package:ayosurabaya/models/data.dart';

class Article {
  String _status;
  int _kode;
  List<Data> _data;
  String _top;
  String _toplink;
  String _bottom;
  String _bottomlink;

  Article(
      {String status,
        int kode,
        List<Data> data,
        String top,
        String toplink,
        String bottom,
        String bottomlink}) {
    this._status = status;
    this._kode = kode;
    this._data = data;
    this._top = top;
    this._toplink = toplink;
    this._bottom = bottom;
    this._bottomlink = bottomlink;
  }

  String get status => _status;
  set status(String status) => _status = status;
  int get kode => _kode;
  set kode(int kode) => _kode = kode;
  List<Data> get data => _data;
  set data(List<Data> data) => _data = data;
  String get top => _top;
  set top(String top) => _top = top;
  String get toplink => _toplink;
  set toplink(String toplink) => _toplink = toplink;
  String get bottom => _bottom;
  set bottom(String bottom) => _bottom = bottom;
  String get bottomlink => _bottomlink;
  set bottomlink(String bottomlink) => _bottomlink = bottomlink;

  Article.fromJson(Map<String, dynamic> json) {
    _status = json['status'];
    _kode = json['kode'];
    if (json['data'] != null) {
      _data = new List<Data>();
      json['data'].forEach((v) {
        _data.add(new Data.fromJson(v));
      });
    }
    _top = json['top'];
    _toplink = json['toplink'];
    _bottom = json['bottom'];
    _bottomlink = json['bottomlink'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this._status;
    data['kode'] = this._kode;
    if (this._data != null) {
      data['data'] = this._data.map((v) => v.toJson()).toList();
    }
    data['top'] = this._top;
    data['toplink'] = this._toplink;
    data['bottom'] = this._bottom;
    data['bottomlink'] = this._bottomlink;
    return data;
  }
}
