import 'dart:async';
import 'dart:convert';
import 'package:ayosurabaya/models/ArticleCategory.dart';
import 'package:ayosurabaya/models/Kanal.dart';
import 'package:ayosurabaya/models/ListData.dart';
import 'package:ayosurabaya/models/Artikel.dart';

import 'package:http/http.dart' as http;

class API {
  static const baseUrl = "https://www.ayosurabaya.com/api_mob/";
  static const URL_IMAGE = "https://www.ayosurabaya.com/images-surabaya/post/articles/";
  static Future<ListData> lisdata() async {
    var client = new http.Client();
    var url = baseUrl + "/getAll";
    var response = await client.get(url);
    var decodeJson = jsonDecode(response.body);
    print(decodeJson);
    return ListData.fromJson(decodeJson);
  }

  static Future<Kanal> fetchGetCategory() async {
    var client = new http.Client();
    var url = baseUrl+"/getKanal";
    var response = await client.get(url);
    var decodeJson = jsonDecode(response.body);
    return Kanal.fromJson(decodeJson);
  }

  static Future<Artikel> fetchPosArtikel() async {
    var client = new http.Client();
    var url = baseUrl+"/getArticle";
    var response = await client.post(url);
    var decodeJson = jsonDecode(response.body);
    return Artikel.fromJson(decodeJson);
  }
  static Future<ArticleCategory> fetchArticleCategory(category, limit, page) async {
    var client = new http.Client();
    var url = baseUrl+"/getRecentArticle";
    var response = await client.post(url, body: {'cat_id' : category, 'limit' : '$limit', 'page' : '$page'});
//    print('Response status: ${response.statusCode}');
    var decodeJson = jsonDecode(response.body);
    print(decodeJson);
    return ArticleCategory.fromJson(decodeJson);
  }

}
