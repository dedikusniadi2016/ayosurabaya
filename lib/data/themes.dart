import 'package:flutter/material.dart';

enum MyTheme { light, dark }

class ThemeNotifier with ChangeNotifier {
  static const MaterialColor white = const MaterialColor(
    0xFFFFFFFF,
    const <int, Color>{
      50: const Color(0xFFFFFFFF),
      100: const Color(0xFFFFFFFF),
      200: const Color(0xFFFFFFFF),
      300: const Color(0xFFFFFFFF),
      400: const Color(0xFFFFFFFF),
      500: const Color(0xFFFFFFFF),
      600: const Color(0xFFFFFFFF),
      700: const Color(0xFFFFFFFF),
      800: const Color(0xFFFFFFFF),
      900: const Color(0xFFFFFFFF),
    },
  );

  static const MaterialColor black = const MaterialColor(
    0x00000000,
    const <int, Color>{
      50: const Color(0x00000000),
      100: const Color(0x00000000),
      200: const Color(0x00000000),
      300: const Color(0x00000000),
      400: const Color(0x00000000),
      500: const Color(0x00000000),
      600: const Color(0x00000000),
      700: const Color(0x00000000),
      800: const Color(0x00000000),
      900: const Color(0x00000000),
    },
  );

  static List<ThemeData> themes = [
    ThemeData(
      brightness: Brightness.light,
      primarySwatch: white,
      accentColor: Colors.cyan[600],
    ),
    ThemeData(
      brightness: Brightness.dark,
      primarySwatch: white,
      accentColor: Colors.cyan[600],
    ),
  ];

  MyTheme _current = MyTheme.light;
  ThemeData _currentTheme = themes[0];

  void switchTheme(bool state) {
    if(!state){
      currentTheme = MyTheme.light;
    } else {
      currentTheme = MyTheme.dark;
    }
  }

  set currentTheme(theme) {
    if (theme != null) {
      _current = theme;
      _currentTheme = _current == MyTheme.light ? themes[0] : themes[1];
      notifyListeners();
    }
  }

  get currentTheme => _current;
  get curretThemeData => _currentTheme;
}
