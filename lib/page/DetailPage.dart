import 'package:ayosurabaya/Widget/article.dart';
import 'package:ayosurabaya/database/DBProvider.dart';
import 'package:ayosurabaya/models/Bookmark.dart';
import 'package:ayosurabaya/plugins/Plugins.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:dio/dio.dart';
import 'dart:convert';
import 'package:url_launcher/url_launcher.dart';
import 'package:connectivity/connectivity.dart';
import 'dart:async';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:share/share.dart';
import 'package:ayosurabaya/models/artikel_detail.dart';
import 'package:ayosurabaya/utility/other.dart';
import 'package:ayosurabaya/API/api.dart';

class DetailPage extends StatefulWidget {
  final String id;
  final String tag;
  String image;
  String category;

  DetailPage({this.id, this.tag, this.image, this.category});

  @override
  _ContentPageState createState() => _ContentPageState();
}

enum TtsState { playing, stopped }

class _ContentPageState extends State<DetailPage>
    with TickerProviderStateMixin {
  String _connectionStatus = 'Unknown';
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;
  bool _status = false;

  ArticleDetails _details;

  String _image;
  String _category;
  bool _collapse = false;

  double _sizeFont = 16.0;

  final snackBar = SnackBar(content: Text("Koneksi internet terputus"));

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  AnimationController _controller;
  Animation<double> _animation;

  Color _iconColor = Colors.white;

  String animals;

  @override
  void initState() {
    super.initState();

    initConnectivity();
    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);

    _controller = AnimationController(
        duration: const Duration(milliseconds: 1000), vsync: this);
    _animation = CurvedAnimation(parent: _controller, curve: Curves.easeIn);
    _controller.forward();
  }

  Future<void> initConnectivity() async {
    ConnectivityResult connectionStatus;

    try {
      connectionStatus = await (Connectivity().checkConnectivity());
    } on PlatformException catch (e) {
      print(e.toString());
      _connectionStatus = "Internet connectivity failed";
    }

    if (!mounted) {
      return;
    }

    _updateConnectionStatus(connectionStatus);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    _connectionStatus = result.toString();
    print("InitConnectivity : $_connectionStatus");
    if (result == ConnectivityResult.mobile ||
        result == ConnectivityResult.wifi) {
      setState(() {
        _status = true;
      });
    } else {
      _status = true;
      _scaffoldKey.currentState.showSnackBar(snackBar);
      print("You are not connected to internet");
    }
  }

  Future<ArticleDetails> _fetchGetArticle() async {
    print("id " + widget.id);
    Response response = await Dio().post(
      "${Other.BASE_URL}getArticle",
      data: FormData.fromMap({"id": widget.id}),
    );
    var decodeJson = jsonDecode(response.toString());
    if (identical(ArticleDetails.fromJson(decodeJson).kode, 200)) {
      _details = ArticleDetails.fromJson(decodeJson);

      if (widget.image == null) {
        setState(() {
          widget.image =
              "${API.URL_IMAGE}${DateFormat('yyyy').format(DateTime.parse(_details.result[0].postDateCreated))}/${DateFormat('MM').format(DateTime.parse(_details.result[0].postDateCreated))}/${DateFormat('dd').format(DateTime.parse(_details.result[0].postDateCreated))}/${_details.result[0].postId}/${_details.result[0].postImageContent}";

          widget.category = _details.result[0].categoryName;
        });
      }
    }

    return _details;
  }

  Widget _createHtmlView(BuildContext context) {
    return Html(
      data: "<p align=\"justify\">${_details.result[0].postContent}</p>",
      padding: const EdgeInsets.only(
        left: 5.0,
      ),
      onLinkTap: (url) {
        String _url = url.substring(0, url.lastIndexOf("/")) + "";
        String id = _url.substring(_url.lastIndexOf("/") + 1);
        if (url.startsWith("https://www.ayosurabaya.com/")) {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (BuildContext context) => DetailPage(
                id: id,
                tag: "category_$id",
              ),
            ),
          );
        } else {
          _launchURL(url);
        }
      },
      useRichText: true,
      defaultTextStyle: TextStyle(fontSize: _sizeFont),
    );
  }

  Widget _futureBuilderContent(BuildContext context) => FutureBuilder(
      future: _fetchGetArticle(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.waiting:
            return Center(child: CircularProgressIndicator());
          case ConnectionState.done:
            return _details == null
                ? Container(
                    child: Center(
                      child: Text(
                        "data tidak ditemukan",
                        textAlign: TextAlign.center,
                      ),
                    ),
                  )
                : _createListContent(context);
          default:
            if (snapshot.hasError)
              return Center(child: Text("Error : ${snapshot.error}"));
        }
        return Container();
      });

  _displaySnackBar(BuildContext context) {
    final snackBar = SnackBar(
        content: Text('Artikel berhasil disimpan di halaman bookmarks'));
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  _createListContent(BuildContext context) {
    return ListView(
      scrollDirection: Axis.vertical,
      children: <Widget>[
        Hero(
          tag: widget.tag,
          child: widget.image == null
              ? CircularProgressIndicator()
              : Plugins.cacheNetworkImage(
              widget.image,
              BoxFit.fill,
              250.0,
              MediaQuery.of(context).size.width),
        ),
        _details.result[0].postImageCaption == null
            ? Container()
            : Container(
                margin: EdgeInsets.only(bottom: 15.0, top: 8.0),
                child: Text(
                  _details.result[0].postImageCaption,
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 11.0),
                )),

        Container(
            margin: EdgeInsets.only(bottom: 15.0, top: 8.0),
            padding: EdgeInsets.only(left: 5.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.only(right: 5.0),
                  child: Icon(
                    Icons.person,
                    size: 14.0,
                  ),
                ),
                Text(
                  _details.result[0].reporter.toUpperCase(),
                  style: TextStyle(fontSize: 12.0, fontWeight: FontWeight.bold),
                )
              ],
            )),

        Padding(
          padding: const EdgeInsets.only(left: 5.0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Image.asset('assets/logo-20.png'),
              Text(
                _details.result[0].categoryName,
                style: TextStyle(
                  color: Colors.red,
                  fontSize: 14.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 20.0, left: 5.0),
          child: Text(
            _details.result[0].postTitle,
            style: TextStyle(fontSize: 22.0, fontWeight: FontWeight.bold),
          ),
        ),

        Container(
            margin: EdgeInsets.only(bottom: 15.0, top: 8.0),
            padding: EdgeInsets.only(left: 5.0),
            child: Text(
              "${Plugins.postTime(_details.result[0])} WIB",
              style: TextStyle(fontSize: 12.0),
            )),

        _details.top == null || _details.top == '' ? Container () : Container(
          margin: EdgeInsets.only(top: 15.0),
          height: 40.0,
          child: Plugins.cacheNetworkImage(
              _details.top,
              BoxFit.fitWidth,
              MediaQuery.of(context).size.height,
              MediaQuery.of(context).size.width),
        ),
        _createHtmlView(context),
        Container(
            margin: EdgeInsets.only(top: 8.0),
            padding: EdgeInsets.only(left: 5.0),
            child: Text(
              "Editor",
              style: TextStyle(fontSize: 11.0),
            )),
        Container(
            margin: EdgeInsets.only(bottom: 15.0),
            padding: EdgeInsets.only(left: 5.0),
            child: Text(
              _details.result[0].author,
              style: TextStyle(fontSize: 14.0, fontWeight: FontWeight.bold),
            )),

        
        _details.bottom == null || _details.bottom == '' ? Container() : Container(
          height: 40.0,
          child: Plugins.cacheNetworkImage(
              _details.bottom,
              BoxFit.fitWidth,
              MediaQuery.of(context).size.height,
              MediaQuery.of(context).size.width),
        ),
        identical(_details.related.kode, 502) == true
            ? Container()
            : _createTitle("Berita Terkait"),
        identical(_details.related.kode, 502) == true
            ? Container()
            : Article(
                warp: true,
                data: _details.related.data,
              ),
        identical(_details.newsCategory.kode, 502) == true
            ? Container()
            : _createTitle(widget.category),
        identical(_details.newsCategory.kode, 502) == true
            ? Container()
            : Article(
                warp: true,
                data: _details.newsCategory.data,
              ),
      ],
    );
  }

  Widget _createTitle(String title) {
    return Container(
      margin: EdgeInsets.only(top: 25.0, bottom: 5.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(bottom: 10.0, left: 10.0),
            child: Text(
              title,
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),
            ),
          ),
          Divider(
            height: 3.0,
            color: Colors.black45,
          ),
        ],
      ),
    );
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw "Could not launch $url";
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          centerTitle: true,
          iconTheme: IconThemeData(color: Color.fromRGBO(38, 153, 251, 1)),
          title: Image.asset('assets/ayosurabaya.png',
              height: 500.0, width: 200.0, alignment: FractionalOffset.center),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.bookmark),
              onPressed: () async {
                await DBProvider.db.setBookmark(Bookmark(
                    postId: _details.result[0].postId,
                    postTitle: _details.result[0].postTitle,
                    postImage: widget.image));
                _displaySnackBar(context);
                _iconColor = Colors.red;
              },
            ),
            IconButton(
              icon: Icon(Icons.text_fields, color: Colors.red),
              onPressed: () {
                if(_sizeFont == 16.0){
                  _sizeFont = 20.0;
                } else {
                  _sizeFont = 16.0;
                }
                setState(() {

                });
              },
            ),
          ]),
      key: _scaffoldKey,
      body: _status == true
          ? _details == null
              ? _futureBuilderContent(context)
              : _createListContent(context)
          : Container(
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(Icons.signal_wifi_off),
                    Text(
                      "Connection Failed",
                      style: TextStyle(fontFamily: 'Montserrat'),
                    ),
                  ],
                ),
              ),
            ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Share.share(
              "${Other.URL_WEBSITE}${DateFormat('yyyy').format(DateTime.parse(_details.result[0].postDateCreated))}/${DateFormat('MM').format(DateTime.parse(_details.result[0].postDateCreated))}/${DateFormat('dd').format(DateTime.parse(_details.result[0].postDateCreated))}/${_details.result[0].postId}/${_details.result[0].slug}");
        },
        child: Icon(Icons.share, color: Colors.white),
        backgroundColor: Colors.blue,
      ),
    );
  }

  @override
  void dispose() {
    _connectivitySubscription.cancel();
    super.dispose();
  }
}
