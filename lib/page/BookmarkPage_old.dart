import 'package:ayosurabaya/database/DBProvider.dart';
import 'package:ayosurabaya/models/Bookmark.dart' as model;
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:ayosurabaya/data/uiset.dart';
import 'package:ayosurabaya/data/themes.dart';
import 'package:provider/provider.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

void main() => runApp(MultiProvider(providers: [
      ChangeNotifierProvider(builder: (_) => UiSet()),
      ChangeNotifierProvider(builder: (_) => ThemeNotifier()),
    ], child: Bookmark()));

class Bookmark extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Slide Tile',
      debugShowCheckedModeBanner: false,
      theme: Provider.of<ThemeNotifier>(context).curretThemeData,
      home: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Image.asset('assets/ayosurabaya.png',
              height: 500.0, width: 200.0, alignment: FractionalOffset.center),
        ),
        body: ProfileBanner(),
      ),
    );
  }
}

class ProfileBanner extends StatelessWidget {
  get context => null;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder(
        future: DBProvider.db.getAllBookmark(),
        builder: (BuildContext context,
            AsyncSnapshot snapshot) {
          print("bookmark ${snapshot.hasData}");
          if (snapshot.hasData) {
            return ListView.builder(
                itemCount: snapshot.data.length,
                itemBuilder: (BuildContext context, int index) {
                  model.Bookmark item = snapshot.data[index];

                  return Slidable(
                    actionPane: SlidableDrawerActionPane(),
                    child: Container(
                      child: ListTile(
                        leading:
                            Image.network(item.postImage, fit: BoxFit.cover),
                        trailing: Icon(Icons.arrow_left),
                        title: Text(
                          item.postTitle,
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                        ),
                      ),
                    ),
                    secondaryActions: <Widget>[
                      IconSlideAction(
                        caption: 'delete',
                        color: Colors.red,
                        icon: Icons.restore_from_trash,
                        onTap: () => {

                        },
                      ),
                    ],
                  );
                });
          }
          return Container();
        },
      ),
    );
  }

  _onAlertButtonsPressed(context) {
    Alert(
      context: context,
      type: AlertType.error,
      title: "Hapus Artikel",
      desc: "Yakin ingin menghapus 1 Artikel tersimpan",
      buttons: [
        DialogButton(
            child: Text(
              "Hapus",
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
            onPressed: () => Navigator.pop(context),
            color: Colors.red),
        DialogButton(
          child: Text(
            "urungkan",
            style: TextStyle(color: Colors.black, fontSize: 20),
          ),
          onPressed: () => Navigator.pop(context),
          color: Colors.white,
        )
      ],
    ).show();
  }
}
