import 'package:ayosurabaya/page/PhotoPage.dart';
import 'package:ayosurabaya/page/search.dart';
import 'package:flutter/material.dart';
import 'package:ayosurabaya/page/home.dart';
import 'package:ayosurabaya/page/CategoryPage.dart';
import 'package:ayosurabaya/page/setting.dart';


// ignore: must_be_immutable
class DynamicArticlePage extends StatelessWidget {
  int page;
  String category;

  DynamicArticlePage({this.page, this.category});
  @override
  Widget build(BuildContext context) {
    
   return page == 0 ? Home() : CategoryPage(category: category);
  }
}
