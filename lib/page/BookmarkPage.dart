import 'package:ayosurabaya/BLoC/BookmarkBLoC.dart';
import 'package:ayosurabaya/database/DBProvider.dart';
import 'package:ayosurabaya/models/Bookmark.dart';
import 'package:ayosurabaya/plugins/Plugins.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

class BookmarkPage extends StatefulWidget {
  @override
  _BookmarkPageState createState() => _BookmarkPageState();
}

class _BookmarkPageState extends State<BookmarkPage> {
  final _bookmarkBloc = BookmarkBloc();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    _bookmarkBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Image.asset('assets/ayosurabaya.png',
            height: 500.0, width: 200.0, alignment: FractionalOffset.center),
      ),
      body: StreamBuilder(
        stream: _bookmarkBloc.bookmarks,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          print("bookmark ${snapshot.hasData}");
          if (snapshot.hasData) {
            return ListView.builder(
                itemCount: snapshot.data.length,
                itemBuilder: (BuildContext context, int index) {
                  Bookmark item = snapshot.data[index];
                  return Slidable(
                    closeOnScroll: true,
                    actionPane: SlidableDrawerActionPane(),
                    child: Container(

                      child: ListTile(
                        leading:
                            Plugins.cacheNetworkImage(item.postImage, BoxFit.cover, 100.0, 100.0),
                        trailing: Icon(Icons.arrow_left),
                        title: Text(
                          item.postTitle,
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                        ),
                      ),
                    ),
                    secondaryActions: <Widget>[
                      IconSlideAction(
                        caption: 'delete',
                        color: Colors.red,
                        icon: Icons.restore_from_trash,
                        onTap: () => {
                          _bookmarkBloc.deleteBookmark(item.id)
                        },
                      ),
                    ],
                  );
                });
          }
          return Container();
        },
      ),
    );
  }
}
