import 'package:ayosurabaya/main.dart';
import 'package:ayosurabaya/page/DetailPage.dart';
import 'package:ayosurabaya/page/setting.dart';
import 'package:ayosurabaya/plugins/Plugins.dart';
import 'package:ayosurabaya/utility/other.dart';
import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:ayosurabaya/API/api.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:ayosurabaya/data/uiset.dart';
import 'package:ayosurabaya/data/themes.dart';
import 'package:provider/provider.dart';
import 'package:ayosurabaya/data/uiset.dart';
import 'package:ayosurabaya/data/themes.dart';
import 'package:ayosurabaya/page/PhotoPage.dart';

void main() => runApp(MultiProvider(providers: [
      ChangeNotifierProvider(builder: (_) => UiSet()),
      ChangeNotifierProvider(builder: (_) => ThemeNotifier()),
    ], child: SearchList()));

class SearchList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'ayo jakarta',
      debugShowCheckedModeBanner: false,
      theme: Provider.of<ThemeNotifier>(context).curretThemeData,
      home: ExamplePage(),
    );
  }
}

class ExamplePage extends StatefulWidget {
  @override
  _ExamplePageState createState() => new _ExamplePageState();
}

class _ExamplePageState extends State<ExamplePage> {
  final TextEditingController _filter = new TextEditingController();
  final dio = new Dio();
  String _searchText = "";
  List post_titles = new List();
  List filteredTitle = new List();
  Icon _searchIcon = new Icon(Icons.search);
  Widget _appBarTitle = Image.asset('assets/ayosurabaya.png',
      height: 500.0, width: 200.0, alignment: FractionalOffset.center);

  _ExamplePageState() {
    _filter.addListener(() {
      if (_filter.text.isEmpty) {
        setState(() {
          _searchText = "";
          filteredTitle = post_titles;
        });
      } else {
        setState(() {
          _searchText = _filter.text;
        });
      }
    });
  }

  @override
  void initState() {
    this._getNames();
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildBar(context),
      body: Material(child:  _buildList(),),
      resizeToAvoidBottomPadding: false,
      bottomNavigationBar: BottomAppBar(
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Expanded(
              child: IconButton(
                icon: Icon(Icons.home),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => BodyLayout()),
                  );
                },
              ),
            ),
            Expanded(
              child: IconButton(
                icon: Icon(Icons.photo),
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => PhotoGalleryPage()));
                },
              ),
            ),
            Expanded(
              child: IconButton(
                icon: Icon(Icons.settings),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => SettingPage()),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildBar(BuildContext context) {
    return new AppBar(
      centerTitle: true,
      title: _appBarTitle,
      iconTheme: IconThemeData(color: Color.fromRGBO(38, 153, 251, 1)),
      leading: new IconButton(
        icon: _searchIcon,
        onPressed: _searchPressed,
      ),
    );
  }

  Widget _cacheNetworkImage(
          String imageUrl, BoxFit fit, double height, double width) =>
      CachedNetworkImage(
        imageUrl: imageUrl,
        placeholder: (context, url) => Image.asset('assets/loading_image.png'),
        errorWidget: (context, url, error) => new Icon(Icons.error),
        fadeInDuration: Duration(seconds: 1),
        fadeOutDuration: Duration(seconds: 1),
        height: height,
        width: width,
        fit: fit,
      );

  Widget _buildList() {
    if (!(_searchText.isEmpty)) {
      List tempList = new List();
      for (int i = 0; i < filteredTitle.length; i++) {
        if (filteredTitle[i]['post_title']
            .toLowerCase()
            .contains(_searchText.toLowerCase())) {
          tempList.add(filteredTitle[i]);
        }
      }
      filteredTitle = tempList;
    }
    return ListView.builder(
      itemCount: post_titles == null ? 0 : filteredTitle.length,
      itemBuilder: (BuildContext context, int index) {
        return Padding(
          padding: EdgeInsets.only(left: 2.0, right: 2.0),
          child: Column(
            children: <Widget>[
              InkWell(
                onTap: () {
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (BuildContext context) => DetailPage(
                        id: filteredTitle[index]['post_id'],
                        tag: filteredTitle[index]['post_id'],
                        category: filteredTitle[index]['category_name'].toUpperCase(),
                        image:
                        "${Other.URL_IMAGE}${DateFormat('yyyy').format(DateTime.parse(filteredTitle[index]['post_date_created']))}/${DateFormat('MM').format(DateTime.parse(filteredTitle[index]['post_date_created']))}/${DateFormat('dd').format(DateTime.parse(filteredTitle[index]['post_date_created']))}/${filteredTitle[index]['post_id']}/${filteredTitle[index]['post_image_content']}",
                      )));
                },
                child: Hero(
                    tag: "category_$index",
                    child: _buildListItem(filteredTitle, index, context)),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 5.0),
                child: Divider(
                  height: 1.0,
                  color: Colors.black26,
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  Widget _buildListItem(List filteredTitle, int index, BuildContext context) => Material(
    type: MaterialType.transparency,
    child: Container(
      height: 100.0,
      padding: const EdgeInsets.only(
        top: 5.0,
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 2.0, left: 2.0),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        margin: const EdgeInsets.only(right: 5.0),
                      ),
                      Image.asset('assets/logo-20.png'),
                      Text(
                        filteredTitle[index]['category_name'].toUpperCase(),
                        style: TextStyle(
                          color: Colors.red,
                          fontSize: 12.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  child: Padding(
                    padding: const EdgeInsets.only(right: 1.0, left: 6.0),
                    child: SizedBox(
                      height: 38.0,
                      child: Text(
                        filteredTitle[index]['post_title'],
                        style: TextStyle(
                          fontSize: 16.0,
                          fontWeight: FontWeight.bold,
                        ),
                        overflow: TextOverflow.ellipsis,
                        maxLines: 2,
                      ),
                    ),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(top: 10.0, left: 6.0),
                  child: Material(
                    type: MaterialType.transparency,
                    child: RichText(
                      text: TextSpan(
                        style: DefaultTextStyle.of(context).style,
                        children: <TextSpan>[
                          filteredTitle[index]['author'] == null
                              ? TextSpan()
                              : TextSpan(children: <TextSpan>[
                            TextSpan(
                              text: "Oleh ",
                              style: TextStyle(
                                fontSize: 11.0,
                              ),
                            ),
                            TextSpan(
                                text: "${filteredTitle[index]['author']} ",
                                style: TextStyle(
                                  fontSize: 11.0,
                                  fontWeight: FontWeight.bold,
                                )),
                          ]),
                          TextSpan(
                            text: Plugins.getDateBetween(filteredTitle[index]['post_date']),
                            style: TextStyle(
                              fontSize: 11.0,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.all(6.0),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(10),
              child: Image.network(
                "${Other.URL_IMAGE}${DateFormat('yyyy').format(DateTime.parse(filteredTitle[index]['post_date_created']))}/${DateFormat('MM').format(DateTime.parse(filteredTitle[index]['post_date_created']))}/${DateFormat('dd').format(DateTime.parse(filteredTitle[index]['post_date_created']))}/${filteredTitle[index]['post_id']}/${filteredTitle[index]['post_image_content']}",
                height: 110.0,
                width: 100.0,
                fit: BoxFit.fill,
              ),
            ),
          ),
        ],
      ),
    ),
  );

  void _searchPressed() {
    setState(() {
      if (this._searchIcon.icon == Icons.search) {
        this._searchIcon = new Icon(Icons.close);
        this._appBarTitle = new TextField(
          controller: _filter,
          decoration: new InputDecoration(
              prefixIcon: new Icon(Icons.search), hintText: 'Search...'),
        );
      } else {
        this._searchIcon = new Icon(Icons.search);
        this._appBarTitle = Image.asset('assets/ayosurabaya.png',
            height: 500.0, width: 200.0, alignment: FractionalOffset.center);
        filteredTitle = post_titles;
        _filter.clear();
      }
    });
  }

  void _getNames() async {
    final response =
        await dio.get('https://www.ayosurabaya.com/api_mob/getSearch');
    List tempList = new List();
    for (int i = 0; i < response.data['data'].length; i++) {
      tempList.add(response.data['data'][i]);
    }
    setState(() {
      post_titles = tempList;
      post_titles.shuffle();
      filteredTitle = post_titles;
    });
  }
}
