import 'package:ayosurabaya/Widget/article.dart';
import 'package:ayosurabaya/page/home.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class SwitchPage extends StatelessWidget {
  int category;

  SwitchPage({@required this.category});

  @override
  Widget build(BuildContext context) {
    return category == 0 ? Home() : Article(
      warp: false,
      load: true,
      refresh: true,
      category: category,
    );
  }
}
