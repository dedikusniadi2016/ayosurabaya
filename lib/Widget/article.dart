import 'package:ayosurabaya/API/api.dart';
import 'package:ayosurabaya/models/ArticleCategory.dart';
import 'package:ayosurabaya/models/Artikel.dart';
import 'package:ayosurabaya/page/DetailPage.dart';
import 'package:ayosurabaya/plugins/Plugins.dart';
import 'package:ayosurabaya/utility/other.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class Article extends StatefulWidget {
  final List<Artikel> data;
  final bool warp;
  final bool refresh;
  final bool load;
  final int category;

  const Article(
      {Key key,
      this.data,
      @required this.warp,
      this.refresh = false,
      this.load = false,
      this.category})
      : super(key: key);

  @override
  _Article createState() => _Article();
}

class _Article extends State<Article> {
  int _limit = 20;
  int _page = 0;
  int _total = 0;
  int _category;
  List<Artikel> _artikel;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }


  @override
  void didUpdateWidget(Article oldWidget) {
    print("update widget");
    if (!identical(_category, widget.category)) {
      setState(() {
        _page = 0;
        _total = 0;
        _artikel = null;
      });
    }

    super.didUpdateWidget(oldWidget);
  }

  Future<ArticleCategory> getArtikel() async {
    _category = widget.category;
    print(_category);
    ArticleCategory art =
        await API.fetchArticleCategory("${widget.category}", _limit, _page);
    if (identical(art.kode, 200)) {
      _artikel == null ? _artikel = art.data : _artikel.addAll(art.data);

      setState(() {
        _total = _artikel.length;
      });
    }

    return art;
  }

  Future<ArticleCategory> handleRefresh() async {
    _page = 0;
    ArticleCategory art =
        await API.fetchArticleCategory("${widget.category}", _limit, _page);
    if (identical(art.kode, 200)) {
      _artikel = art.data;

      setState(() {
        _total = _artikel.length;
      });
    }

    return art;
  }

  @override
  Widget build(BuildContext context) {
    return widget.category == null
        ? _builderListView(widget.data)
        : _artikel == null
            ? _buildFutureView()
            : widget.refresh ? _refreshIndicator() : _builderListView(_artikel);
  }

  Widget _refreshIndicator() => RefreshIndicator(
        child: _builderListView(_artikel),
        onRefresh: handleRefresh,
      );

  Widget _buildFutureView() => FutureBuilder(
      future: getArtikel(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.waiting:
            return Center(child: CircularProgressIndicator());
          case ConnectionState.done:
            return widget.refresh
                ? _refreshIndicator()
                : _builderListView(_artikel);
          default:
            if (snapshot.hasError)
              return Center(child: Text("Error : ${snapshot.error}"));
        }
        return Container();
      });

  Widget _builderListView(List<Artikel> items) => ListView.builder(
      shrinkWrap: widget.warp,
      scrollDirection: Axis.vertical,
      itemCount: widget.warp ? items.length : _total,
      physics: widget.warp == true
          ? const NeverScrollableScrollPhysics()
          : const ClampingScrollPhysics(), //const AlwaysScrollableScrollPhysics(),
      itemBuilder: (BuildContext context, int index) {
        if (widget.load) {
          if (index >= _total - 1) {
            _page++;
            getArtikel();
          }
        }
        return Padding(
          padding: EdgeInsets.only(left: 2.0, right: 2.0),
          child: Column(
            children: <Widget>[
              InkWell(
              onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => DetailPage(
                                id: items[index].postId,
                                image:
                                    "${Other.URL_IMAGE}${DateFormat('yyyy').format(DateTime.parse(items[index].postDateCreated))}/${DateFormat('MM').format(DateTime.parse(items[index].postDateCreated))}/${DateFormat('dd').format(DateTime.parse(items[index].postDateCreated))}/${items[index].postId}/${items[index].postImageContent}",
                                category: items[index].categoryName,
                                tag: items[index].postId)));
                  },                child: Hero(
                    tag: index,
                    child: _buildListItem(items[index], index, context)),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 5.0),
                child: Divider(
                  height: 1.0,
                  color: Colors.black26,
                ),
              ),
            ],
          ),
        );
      });

  Widget _buildListItem(Artikel article, int index, BuildContext context) =>
      Material(
        child: Container(
          height: 100.0,
          padding: const EdgeInsets.only(
            top: 5.0,
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 2.0, left: 2.0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            margin: const EdgeInsets.only(right: 5.0),
                            child: Icon(
                              Icons.category,
                              size: 11.0,
                              color: Colors.blueAccent,
                            ),
                          ),
                          Text(
                            article.categoryName.toUpperCase(),
                            style: TextStyle(
                              fontSize: 11.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      child: Padding(
                        padding: const EdgeInsets.only(right: 1.0, left: 2.0),
                        child: SizedBox(
                          height: 38.0,
                          child: Text(
                            article.postTitle,
                            style: TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.bold,
                            ),
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                          ),
                        ),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(
                          top: 10.0, right: 2.0, left: 2.0),
                      child: RichText(
                        text: TextSpan(
                          style: DefaultTextStyle.of(context).style,
                          children: <TextSpan>[
                            Plugins.textSpanReporter(article.author),
                            TextSpan(
                              text: Plugins.getDateBetween(article.postDate),
                              style: TextStyle(
                                fontSize: 11.0,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.all(6.0),
                child: ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                  child: Plugins.cacheNetworkImage(
                      "${API.URL_IMAGE}${DateFormat('yyyy').format(DateTime.parse(article.postDateCreated))}/${DateFormat('MM').format(DateTime.parse(article.postDateCreated))}/${DateFormat('dd').format(DateTime.parse(article.postDateCreated))}/${article.postId}/${article.postImageContent}",
                      BoxFit.fill,
                      100.0,
                      100.0),
                ),
                ),
//              Container(
//                  margin: const EdgeInsets.only(top: 8.0, right: 3.0),
//                  child: InkWell(
//                    onTap: () {},
//                    child: Icon(
//                      Icons.share,
//                      size: 21.0,
//                      color: Colors.black54,
//                    ),
//                  )),
            ],
          ),
        ),
      );

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }
}
