import 'dart:async';

import 'package:ayosurabaya/database/DBProvider.dart';
import 'package:ayosurabaya/models/Bookmark.dart';

class BookmarkBloc{

  BookmarkBloc(){
    getBookmarks();
  }

  final _bookmarkController = StreamController<List<Bookmark>>.broadcast();
  get bookmarks => _bookmarkController.stream;

  dispose(){
    _bookmarkController.close();
  }

  getBookmarks() async {
    _bookmarkController.sink.add(await DBProvider.db.getAllBookmark());
  }

  deleteBookmark(int id){
    DBProvider.db.deleteBookmark(id);
    getBookmarks();
  }
}