import 'dart:io';

import 'package:ayosurabaya/models/Bookmark.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class DBProvider {
  DBProvider._();
  static final DBProvider db = DBProvider._();

  Database _database;

  static const TABLE = "bookmark";
  static const ID = "id";
  static const POST_ID = "post_id";
  static const POST_TITLE = "post_title";
  static const POST_IMAGE = "post_image";

  Future<Database> get database async {
    if (_database != null) return _database;
    // if _database is null we instantiate it
    _database = await initDB();
    return _database;
  }

  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "ayosurabaya.db");
    return await openDatabase(path, version: 1, onOpen: (db) {},
        onCreate: (Database db, int version) async {
          await db.execute("CREATE TABLE $TABLE ("
              "$ID INTEGER PRIMARY KEY AUTOINCREMENT,"
              "$POST_ID TEXT,"
              "$POST_TITLE TEXT,"
              "$POST_IMAGE TEXT"
              ")");
        });
  }
  
  setBookmark(Bookmark bookmark) async{
    final db = await database;
//    var res = await db.rawInsert("INSERT Into bookmark (id,post_id,post_title,post_image)"
//        " VALUES (${bookmark.id},${bookmark.postId},${bookmark.postTitle},${bookmark.postImage})");
    var res = await db.insert(TABLE, bookmark.toMap());
    return res;
  }

  getBookmark(int id) async {
    final db = await database;
    var res =await  db.query(TABLE, where: "$ID = ?", whereArgs: [id]);
    return res.isNotEmpty ? Bookmark.fromJson(res.first) : Null ;
  }

  getAllBookmark() async {
    final db = await database;
    var res = await db.query(TABLE);
    List<Bookmark> list = res.isNotEmpty ? res.map((item) => Bookmark.fromJson(item)).toList() : [];
    return list;
  }

  deleteBookmark(int id) async {
    final db = await database;
    db.delete(TABLE, where: "$ID = ?", whereArgs: [id]);
  }

  deleteAll() async {
    final db = await database;
    db.rawDelete("Delete * from $TABLE");
  }

}