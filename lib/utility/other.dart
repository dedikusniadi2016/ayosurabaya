class Other{
  static const URL_IMAGE = "https://www.ayosurabaya.com/images-surabaya/post/articles/";
  static const URL_PHOTOS = "https://www.ayosurabaya.com/images-surabaya/post/photos/";
  static const BASE_URL = "https://www.ayosurabaya.com/api_mob/";
  static const URL_WEBSITE = "https://www.ayosurabaya.com/read/";
  static const URL_READ_PHOTO = "https://www.ayosurabaya.com/view/";
}