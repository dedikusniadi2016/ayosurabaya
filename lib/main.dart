import 'dart:async';
import 'dart:convert';
import 'package:ayosurabaya/Widget/switch_page.dart';
import 'package:ayosurabaya/sharedpreferences/sharedpref.dart';
import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:bubble_tab_indicator/bubble_tab_indicator.dart';
import 'package:ayosurabaya/page/DynamicArticlePage.dart';
import 'package:ayosurabaya/page/setting.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ayosurabaya/page/search.dart';
import 'package:ayosurabaya/page/PhotoPage.dart';
import 'package:dio/dio.dart';
import 'package:flutter/services.dart';
import 'models/Kanal.dart';
import 'package:ayosurabaya/utility/other.dart';
import 'package:connectivity/connectivity.dart';
import 'package:ayosurabaya/page/IndexPage.dart';
import 'package:provider/provider.dart';
import 'package:ayosurabaya/data/uiset.dart';
import 'package:ayosurabaya/data/themes.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:ayosurabaya/page/DetailAboutPage.dart';
import 'package:ayosurabaya/models/about.dart';

void main() => runApp(MultiProvider(providers: [
      ChangeNotifierProvider(builder: (_) => UiSet()),
      ChangeNotifierProvider(builder: (_) => ThemeNotifier()),
    ], child: MyApp()));

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: Provider.of<ThemeNotifier>(context).curretThemeData,
      home: BodyLayout(),
    );
  }
}

class BodyLayout extends StatefulWidget {
  @override
  _BodyLayoutState createState() => _BodyLayoutState();
}

class _BodyLayoutState extends State<BodyLayout>
    with SingleTickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  String _connectionStatus = 'Unknown';
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;
  bool _status = false;

  final snackBar = SnackBar(content: Text("Koneksi internet terputus"));

  TabController _tabController;
  Kanal _kanal;
  int _totalKanal = 0;
  List<Tab> tabs = List();

  AboutModel _aboutModel;

  int currentIndex = 0;
  int _counter = 0;
  PageController _pageController;

  Future<void> initConnectivity() async {
    ConnectivityResult connectionStatus;

    try {
      connectionStatus = await (Connectivity().checkConnectivity());
    } on PlatformException catch (e) {
      print(e.toString());
      _connectionStatus = "Internet connectivity failed";
    }

    if (!mounted) {
      return;
    }

    _updateConnectionStatus(connectionStatus);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    _connectionStatus = result.toString();
    print("InitConnectivity : $_connectionStatus");
    if (result == ConnectivityResult.mobile ||
        result == ConnectivityResult.wifi) {
      setState(() {
        _status = true;
      });
    } else {
      _status = true;
      print("You are not connected to internet");
    }
  }

  Future<AboutModel> _fetchGetAboutUs() async {
    Response response =
        await Dio().get("https://www.ayosurabaya.com/api_mob/getPages");

    var decodeJson = jsonDecode(response.toString());
    if (identical(AboutModel.fromJson(decodeJson).kode, 200)) {
      _aboutModel = AboutModel.fromJson(decodeJson);
    }

    return _aboutModel;
  }

  Widget _futureBuilder() {
    return FutureBuilder(
        future: _fetchGetAboutUs(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
              return Center(child: CircularProgressIndicator());
            case ConnectionState.done:
              return _aboutModel == null
                  ? Container(
                      child: Center(
                        child: Text(
                          "data tidak ditemukan",
                          textAlign: TextAlign.center,
                        ),
                      ),
                    )
                  : _buildListView(snapshot.data, context);
            default:
              if (snapshot.hasError)
                return Center(child: Text("Error : ${snapshot.error}"));
          }
          return Container();
        });
  }

  Widget _buildListView(AboutModel data, BuildContext context) => ListView(
        children:
            data.data.map<Widget>((item) => _buildItem(item, context)).toList(),
      );

  _buildItem(Data item, BuildContext context) {
    return GestureDetector(
        onTap: () {
          Navigator.of(context).push(MaterialPageRoute(
              builder: (BuildContext context) =>
                  DetailsAboutPage(title: item.title, text: item.content)));
        },
        child: SizedBox(
          child: Stack(
            children: <Widget>[
              ListTile(
                title: Text(item.title),
              ),
            ],
          ),
        ));
  }

  getKanal() async {
    Response response = await Dio().get("${Other.BASE_URL}getKanal");
    var decodeJson = jsonDecode(response.toString());

    _kanal = Kanal.fromJson(decodeJson);
    _totalKanal = _kanal.category.length;

    _tabController = new TabController(vsync: this, length: _totalKanal);

    for (int i = 0; i < _kanal.category.length; i++) {
      tabs.add(Tab(
        text: _kanal.category[i].categoryId,
      ));
    }

    return _kanal;
  }

  Widget _buildView() => Stack(
        children: <Widget>[
          Container(
            child: TabBarView(
              controller: _tabController,
              children: _kanal.category.map((item) {
                return Container(
                  margin: EdgeInsets.only(top: 45.0),
                  child: DynamicArticlePage(
                    page: int.parse(item.categoryId),
                    category: item.categoryId,
                  ),
                );
              }).toList(),
            ),
          ),
          Align(
            alignment: Alignment.topLeft,
            child: Container(
              height: 45.0,
              child: _buildTabBarCategory(),
            ),
          ),
        ],
      );

  Widget _buildTabBarCategory() => TabBar(
        isScrollable: true,
        unselectedLabelColor: Color.fromRGBO(127, 196, 253, 1),
        labelColor: Colors.white,
        labelStyle: TextStyle(fontWeight: FontWeight.bold),
        indicatorSize: TabBarIndicatorSize.tab,
        indicator: new BubbleTabIndicator(
          indicatorHeight: 35.0,
          indicatorColor: Color.fromRGBO(38, 153, 251, 1),
          tabBarIndicatorSize: TabBarIndicatorSize.tab,
        ),
        tabs: _kanal.category
            .map((item) => Tab(
                    child: Row(
                  children: <Widget>[Text(" ${item.categoryName}")],
                )))
            .toList(),
        controller: _tabController,
      );

  Widget _futureBuilderView() => FutureBuilder(
      future: getKanal(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.waiting:
            return Center(child: CircularProgressIndicator());
          case ConnectionState.done:
            return _kanal == null
                ? Container(
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Icon(Icons.signal_wifi_off),
                          Text(
                            "Connection Failed",
                            style: TextStyle(fontFamily: 'Montserrat'),
                          ),
                        ],
                      ),
                    ),
                  )
                : _buildView();
          default:
            if (snapshot.hasError)
              return Center(child: Text("Error : ${snapshot.error}"));
        }
        return Container();
      });

  Widget pageBuilder() => PageView.builder(
      itemCount: 4,
      controller: _pageController,
      onPageChanged: (index) {
        setState(() {
          currentIndex = index;
        });
      },
      itemBuilder: (BuildContext context, int index) {
        switch (index) {
          case 0:
            return _kanal == null ? _futureBuilderView() : _buildView();
          case 1:
            return PhotoGalleryPage();
          case 2:
            return IndexPage();
          case 3:
            return SettingPage();
        }

        return Container();
      });

  checkMode() {
    SharedPref.getStateDarkMode().then((val) {
      if (val != null) {
        Provider.of<ThemeNotifier>(context).switchTheme(val);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        centerTitle: true,
        iconTheme: IconThemeData(color: Color.fromRGBO(38, 153, 251, 1)),
        title: Image.asset('assets/ayosurabaya.png',
            height: 500.0, width: 200.0, alignment: FractionalOffset.center),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.search,
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => SearchList()),
              );
            },
          ),
        ],
      ),
      key: _scaffoldKey,
      body: _status == true
          ? pageBuilder()
          : Container(
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(Icons.signal_wifi_off),
                    Text(
                      "Connection Failed",
                      style: TextStyle(fontFamily: 'Montserrat'),
                    ),
                  ],
                ),
              ),
            ),
      bottomNavigationBar: BottomNavyBar(
        selectedIndex: currentIndex,
        showElevation: true,
        onItemSelected: (index) => setState(() {
          currentIndex = index;
          _pageController.animateToPage(index,
              duration: Duration(milliseconds: 300), curve: Curves.ease);
        }),
        items: [
          BottomNavyBarItem(
            icon: Icon(Icons.chrome_reader_mode),
            title: Text('Berita'),
            activeColor: Colors.blue[900],
          ),
          BottomNavyBarItem(
              icon: Icon(Icons.photo_library),
              title: Text('Foto'),
              activeColor: Colors.blue[900]),
          BottomNavyBarItem(
              icon: Icon(Icons.indeterminate_check_box),
              title: Text('index'),
              activeColor: Colors.blue[900]),
          BottomNavyBarItem(
              icon: Icon(Icons.settings),
              title: Text('Pengaturan'),
              activeColor: Colors.blue[900]),
        ],
      ),
      drawer: Drawer(
        child: _status == true
            ? _aboutModel == null
                ? _futureBuilder()
                : _buildListView(_aboutModel, context)
            : Container(
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(Icons.signal_wifi_off),
                      Text(
                        "Connection Failed",
                        style: TextStyle(fontFamily: 'Montserrat'),
                      ),
                    ],
                  ),
                ),
              ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    _pageController = PageController(initialPage: currentIndex, keepPage: true);
    checkMode();
    initConnectivity();
    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
  }

  @override
  void dispose() {
    _tabController.dispose();
    _connectivitySubscription.cancel();
    super.dispose();
  }
}
