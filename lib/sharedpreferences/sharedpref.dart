import 'package:shared_preferences/shared_preferences.dart';

class SharedPref{
  static const DARK_MODE = "darkmode";
  static setStateDarkMode(bool status) async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool(DARK_MODE, status);
  }

  static Future<bool> getStateDarkMode() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool on = prefs.getBool(DARK_MODE);
    return on;
  }
}